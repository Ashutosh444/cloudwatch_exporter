Ansible Role: Cloudwatch Exporter
========================
This role to install and setup cloudwatch exporter.

Dependencies
------------
  * java

Role Variables
--------------
```
installation_dir: /opt
version: 0.6.0
port: 1234
```

Example Playbook
----------------
```
---
- name: It will automate Cloudwatch setup
  hosts: server
  become: true
  roles:
    - role: cloudwatch_exporter$ 
...

$  ansible-playbook site.yml -i inventory

```

Inventory
----------
An inventory should look like this:-
```
[server]                 
192.xxx.x.xxx    ansible_user=ubuntu 
```

Future Proposed Changes
-----------------------
- Update elastalert for centos 6 , 7 as well.

Author Information
------------------
```
Name: Ashutosh Mishra
MailID: ashutosh.mishra@opstree.com
```

